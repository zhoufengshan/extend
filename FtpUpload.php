<?php
use think\Config;
class FtpUpload{
    protected static $instance;

    protected $config = [];

    protected $link;

    protected $login;

    /**
     * 构造方法
     * FtpUpload constructor.
     * @param $options
     */
    private function __construct($options)
    {
        if(empty($this->config)){
            $this->config = Config::get('ftp');
        }
        $this->config = array_merge($this->config,is_array($options)?$options:[]);

        $this->link = @ftp_connect($this->config['host'],$this->config['port'],$this->config['timeout']);

        $this->login = ftp_login($this->link, $this->config['username'], $this->config['password']);
    }

    /**
     * 私有克隆
     */
    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

    /**
     * 初始化
     * @param array $options
     * @return static
     */
    public static function instance($options = []){
        if(is_null(self::$instance)){
            self::$instance = new static($options);
        }
        return self::$instance;
    }

    /**
     * ftp上传
     * @param $ftppath
     * @param $filename
     * @param $filePath
     * @param bool $type
     * @return bool
     */
    public function ftpUpload($ftppath,$filename,$filePath,$type = false){
        if($type){
            $this->dMkdir($ftppath);
        }
        $path = ftp_pwd($this->link) . $ftppath;
        if (!ftp_put($this->link,$path.$filename, $filePath, FTP_BINARY)) {
            return false;
        }else{
            return true;
        }
    }

    /**
     * 删除 FTP 服务器上的一个文件
     * @param $filename
     * @return bool
     */
    public function ftpDelete($filename){
        if(ftp_delete($this->link,$filename)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 创建目录
     * @param $dir
     * @return bool
     */
    public function ftpMkdir($dir){
        return ftp_mkdir($this->link,$dir);
    }

    /**
     * 设置选项
     * @param $option
     * @param $value
     * @return bool
     */
    public function ftpSetOption($option,$value){
        return ftp_set_option($this->link,$option,$value);
    }

    /**
     * 重命名
     * @param $from
     * @param $to
     * @return bool
     */
    public function ftpRename($from,$to){
        return ftp_rename($this->link,$from,$to);
    }

    /**
     * 返回指定目录的文件列表
     * @param $dir
     * @return array
     */
    public function ftpNlist($dir){
        return ftp_nlist($this->link,$dir);
    }

    /**
     * 返回指定目录的文件详细列表
     * @param $dir
     * @return array
     */
    public function ftpRawList($dir){
        return ftp_rawlist($this->link,$dir);
    }

    /**
     * 删除目录
     * @param $dir  目录必须是一个空目录的绝对或相对路径
     * @return bool
     */
    public function ftpRmdir($dir){
        return ftp_rmdir($this->link,$dir);
    }

    /**
     * ftp下载
     * @param $local
     * @param $remote
     * @return int
     */
    public function ftpDownload($local,$remote){
        return ftp_nb_get($this->link,$local,$remote,FTP_BINARY,0);
    }

    /**
     * 返回当前目录名称
     * @return string
     */
    public function ftpPwd(){
        return ftp_pwd($this->link);
    }

    /**
     * 创建多级目录
     * @param $path
     * @param bool $isBackRoot
     * @return bool|string
     */
    public function dMkdir($path,$isBackRoot = false){
        $path = trim($path,'/');
        $path_arr = explode('/',$path); // 取目录数组
        $path_div = count($path_arr); // 取层数
        foreach($path_arr as $val) // 创建目录
        {
            if(@ftp_chdir($this->link,$val) == FALSE)
            {
                $tmp = @ftp_mkdir($this->link,$val);
                if($tmp == FALSE)
                {
                    return false;
                }
                @ftp_chdir($this->link,$val);
            }
        }
        if($isBackRoot){
            for($i=1;$i<=$path_div;$i++)
            {
                @ftp_cdup($this->link);
            }
        }
        return $this->ftpPwd();
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        ftp_close($this->link);
    }
}