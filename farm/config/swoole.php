<?php
return  [
    'room' => [
        'host'            => '0.0.0.0',
        'port'            => 9501,
        'worker_num' 	  => 4,
        'max_request' 	  => 10000,
        'task_worker_num' => 16,
        'open_eof_check' => true, //打开EOF检测
        'package_eof' => '\n', //设置EOF
        'open_eof_split' => true, //自动分割数据包
        'heartbeat_check_interval' => 60,
        'heartbeat_idle_time' => 120,
        //'dispatch_mode'     => 3,
        'log_file' => '/var/log/swoole/room.log',
        'daemonize' => true,
        'pid_file' => __DIR__.'/room.pid',
    ]
];