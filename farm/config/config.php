<?php
return [
    'auth_code_key' => 'AF@!!sdfSDAD#78909DCdafasdP1asdfQ2].',

    //域名
    'web_url'       => 'http://kh.com',

    //头像路径
    'avatar_dir'    => '/uploads/avatar/',

    //----------------好友 Swoole--------------------------/
    //uid绑定=>fd连接 [需要用户uid]
    //'login_flag'    => 'uid_bind_fd_',

    //fd连接绑定uid  [需要fd连接id]
    //'fd_flag'       => 'fd_bind_uid_',

    //好友列表 [字符串存储(位置主程序)需要用户uid]
    'friend_list_flag'   => 'friend_list_uids_',

    //用户信息 --->在接口处已初始化保护了，uid,头像，昵称信息
    'user_info_flag'    =>  'user_info_',

    //用户位置信息[所在大厅和所在的游戏大厅的位置]
    'user_location'     =>   'user_location_',

    //-----------------房间 Swoole-------------------------/

    //用户绑定房间当前fd [需要用户uid=> 用户绑定fd] --->在入口文件已做处理
    'user_flag'     => 'user_bind_fd_',

    //当前fd绑定uid [需要fd => 用户UID] --->在入口文件已做处理
    'user_fd_flag'     => 'fd_bind_user_',

    //房间内用户列表[需要房间ID] =>房间绑定用户uid 哈希存 "uid_uid"
    'room_user_list_flag'   => 'room_user_list_',

    //在大厅的用户列表 哈希存 "uid_uid
    'hall_user_flag'     =>  'hall_user_list',

    //用游戏ID做标识
    'user_game_flag'     =>  'user_game_',

    //用大区ID做标识
    'user_zone_flag'     => 'user_zone_',

    //用服务器Id做标识
    'user_server_flag'   => 'user_server_',

];