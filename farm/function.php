<?php
function autoLoader($class){
    $file = PATH.'/src/'.$class.'.php';
    if(is_file($file)){
        return require_once $file;
    }
}
spl_autoload_register("autoLoader");