<?php
class Init {
    public static $config = [];

    /**
     * 初始化
     */
    public static function run(){
        Congfig::load(PATH.'/config/swoole.php','ws');
        Congfig::load(PATH.'/config/database.php','db');
        Congfig::load(PATH.'/config/redis.php','redis');
        Congfig::load(PATH.'/config/config.php');
    }
}