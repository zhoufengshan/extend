<?php
/**
 * Redis Extension
 * Class RedisExt
 */
class RedisExt {
    protected static $instence;
    public $handler;
    public $connect;
    public $select;
    public $config = [
        'host'=>'192.168.2.227',
        'port'=>6379
    ];

    /**
     * 配置
     * RedisExt constructor.
     * @param $option
     * @param $select
     */
    protected function __construct($option,$select)
    {
        if(empty($this->config)){
            $this->config = is_array($option)?$option:[];
        }
        $this->handler = new \Redis();
        $this->connect = $this->handler->connect($this->config['host'],$this->config['port']);
        $this->select = $this->handler->select(is_int($select)?$select:0);
    }

    /**
     * 单例
     * @param array $options
     * @param int $select
     * @return static
     */
    public static function instence($options=[],int $select=0){
        if(is_null(self::$instence)){
            self::$instence = new static($options,$select);
        }
        return self::$instence;
    }

    /**
     * 私有克隆
     */
    protected function __clone()
    {
        // TODO: Implement __clone() method.
    }

    public function set($key,$value){
        return $this->handler->set($key,$value);
    }
    public function get($field){
        return $this->handler->get($field);
    }
    public function hGetAll($field){
        return $this->handler->hGetAll($field);
    }
    public function sAdd($field,$value){
        return $this->handler->sAdd($field,$value);
    }
    public function sRandMember($field,$count=1){
        return $this->handler->sRandMember($field,$count);
    }
}