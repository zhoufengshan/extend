<?php
use think\Config;
/**
 * 配置
 * Class Congfig
 */
class Congfig {
    protected static $config = [];

    /**
     *加载配置
     * @param $file
     * @param string $name
     * @return array
     */
    public static function load($file,string $name = ''){
        if(is_file($file)){
            if($name){
                $name = strtolower($name);
            }
            $path = pathinfo($file,PATHINFO_EXTENSION);
            if($path == 'php'){
                return self::set(include $file,$name);
            }
        }else{
            return self::$config;
        }
    }

    /**
     * 设置配置
     * @param $value
     * @param $name
     * @return array
     */
    public static function set ($value,$name) {
        if($name){
            return self::$config[$name] = $value;
        }else{
            return self::$config = array_merge($value,self::$config);
        }
    }

    /**
     * 读取配置
     * @param string $name
     * @return array|mixed
     */
    public static function get (string $name = '') {
        if($name){
            return self::$config[$name];
        }else{
            return self::$config;
        }
    }
}