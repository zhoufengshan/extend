<?php

/**
 * Class Config
 */

class Config
{
    public $config;
    public static $debug = false;
    protected $config_path = array();
    protected static $active = false;

    /**
     * set debug
     * @param $debug
     * @return bool
     */
    public function setDebug(bool $debug){
        if(is_bool($debug)){
            self::$debug = $debug;
        }
    }

    /**
     * set config path
     * @param $dir
     * @return bool
     */
    public function setPath($dir){
        $_dir = realpath($dir);
        if($_dir == false){
            error:
            if(self::$debug){
                trigger_error("config dir [$dir] is not exists",E_USER_WARNING);
            }
            return false;
        }
        $dir = $_dir;
        if(!is_dir($_dir)){
            goto error;
        }

        if(in_array($dir,$this->config_path)){
            if(self::$debug){
                trigger_error("config dir [$dir] is already added",E_USER_WARNING);
            }
            return false;
        }

        $this->config_path[] = $dir;
        self::$active = true;
        return true;
    }

    /**
     * load config
     * @param $index
     */
    public function load($index){
        foreach ($this->config_path as $path){
            $file = $path.'/'.$index.'.php';
            if(is_file($file)){
                $retData = include_once $file;
                if (empty($retData) and self::$debug){
                    trigger_error(__CLASS__." $file no return data");
                }else{
                    $this->config[$index] = $retData;
                }
            }elseif(self::$debug){
                trigger_error(__CLASS__." $file not exists");
            }
        }
    }

    /**
     * set index
     * @param $index
     * @param $newval
     */
    public function offsetSet($index,$newval){
        $this->config[$index] = $newval;
    }

    /**
     * get index
     * @param $index
     * @return bool
     */
    public function offsetGet($index){
        if(!isset($this->config[$index])){
            $this->load($index);
        }
        return isset($this->config[$index]) ? $this->config[$index] : false;
    }

    /**
     * unset index
     * @param $index
     */
    public function offsetUnset($index){
        unset($this->config[$index]);
    }

    /**
     * index exists
     * @param $index
     * @return bool
     */
    public function offsetExists($index){
        if(!isset($this->config[$index])){
            $this->load($index);
        }
        return isset($this->config[$index]);
    }

}